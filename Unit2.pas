unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VclTee.TeeGDIPlus, VclTee.TeEngine,
  Vcl.ExtCtrls, VclTee.TeeProcs, VclTee.Chart, VclTee.Series, Vcl.StdCtrls,
  ClipBrd, Vcl.Grids;

type
  TForm2 = class(TForm)
    Chart1: TChart;
    Series1: TAreaSeries;
    Button1: TButton;
    StringGrid1: TStringGrid;
    ColorBox1: TColorBox;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    CheckBox1: TCheckBox;
    Button5: TButton;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure ColorBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    procedure PrepareData;
    { Public declarations }
  end;

  TEnergyTimePainter = record
  type
    TElem = record

      time, energy: double;

    end;

  var

    procedure SetTimeEnergy(time, energy: Tarray<double>);
    procedure PlotChart(ch: TChart);

  strict private
    data: Tarray<TElem>;

  end;

var
  Form2: TForm2;
  energyTimePainter: TEnergyTimePainter;
  timeFromExcel, EnergyFromExcel: Tarray<double>;

implementation

{$R *.dfm}

procedure InsertFromExcel(st: TStringGrid);
var
  s: string;
  i, col, row, left, top, from, len: integer;
begin
  s := Clipboard.AsText;
  len := Length(s);
  left := st.Selection.left;
  top := st.Selection.top;
  from := 1;
  col := left;
  row := top;
  for i := 1 to Length(s) do
    case Ord(s[i]) of
      9:
        begin
          st.Cells[col, row] := Copy(s, from, i - from);
          from := i + 1;
          Inc(col);
          if col = st.ColCount then
            st.ColCount := st.ColCount + 1;
        end;
      10:
        begin
          from := i + 1;
          col := left;
          Inc(row);
          if (row = st.RowCount) and (i <> len) then
            st.RowCount := st.RowCount + 1;
        end;
      13:
        st.Cells[col, row] := Copy(s, from, i - from);
    end;
end;

procedure TForm2.Button1Click(Sender: TObject);
begin

  try

    InsertFromExcel(StringGrid1);
    timeFromExcel := [];
    EnergyFromExcel := [];

    for var i := 1 to StringGrid1.RowCount - 1 do
    begin
      timeFromExcel := timeFromExcel + [strtofloat(StringGrid1.Cells[1, i])];
      EnergyFromExcel := EnergyFromExcel +
        [strtofloat(StringGrid1.Cells[2, i])];
    end;

    energyTimePainter.SetTimeEnergy(timeFromExcel, EnergyFromExcel);
    energyTimePainter.PlotChart(Chart1);

  except
    on e: Exception do
    begin
      showmessage('�� ������� ��������');
      exit;
    end;

  end;

end;

procedure TForm2.Button2Click(Sender: TObject);
begin

  var
  titleName := inputBox('�������� �������', ' ', Chart1.Title.Caption);

  Chart1.Title.Caption := titleName;

end;

procedure TForm2.Button3Click(Sender: TObject);
begin

  var
  YAxisTitle := inputBox('������� ��� ��� Y', ' ',
    Chart1.LeftAxis.Title.Caption);

  Chart1.LeftAxis.Title.Caption := YAxisTitle;

end;

procedure TForm2.Button4Click(Sender: TObject);
begin

  var
  XAxisTitle := inputBox('������� ��� ��� X', ' ',
    Chart1.BottomAxis.Title.Caption);

  Chart1.BottomAxis.Title.Caption := XAxisTitle;

end;

procedure TForm2.Button5Click(Sender: TObject);
begin

  var
  maxValueStr := inputBox('������������ �������� Y', ' ',
    Chart1.LeftAxis.Maximum.ToString);

  if maxValueStr = '' then
  begin
    Chart1.LeftAxis.AutomaticMaximum:=true;
    Chart1.LeftAxis.AutomaticMinimum:=false;
  end
  else
  begin

    var
      value: double;

    try

      value := strtofloat(maxValueStr);

    except
      on e: Exception do
      begin
        showmessage('�������� �� �������� ������');
        exit;
      end;

    end;

    Chart1.LeftAxis.AutomaticMaximum := false;
    Chart1.LeftAxis.Maximum := value;

  end;

end;

procedure TForm2.Button6Click(Sender: TObject);
begin

for var I := 1 to StringGrid1.RowCount-1 do
for var j := 1 to StringGrid1.ColCount-1 do
 StringGrid1.Cells[i,j]:='';

end;

procedure TForm2.CheckBox1Click(Sender: TObject);
begin

  (Chart1.SeriesList[0] as TAreaSeries).AreaLinesPen.Visible :=
    CheckBox1.Checked;

end;

procedure TForm2.ColorBox1Change(Sender: TObject);
begin

  Chart1.SeriesList[0].Color := ColorBox1.Selected;
  energyTimePainter.PlotChart(Chart1);

end;

procedure TForm2.PrepareData;
begin

end;

{ TEnergyTimePainter }

procedure TEnergyTimePainter.PlotChart(ch: TChart);
begin

  if ch = nil then
    raise Exception.Create('������ nil chart');

  if Length(self.data) = 0 then
    raise Exception.Create('����������� �������� � data');

  ch.SeriesList[0].Clear;
  ch.SeriesList[0].Color := Form2.ColorBox1.Selected;



  // ch.ClearChart;

  // var
  // Series: TAreaSeries;

  // Series := TAreaSeries.Create(ch);
  // ch.AddSeries(Series);
  // series.AreaColor:=clgreen;
  // series.
  // series.:=clgreen;
  // series.Marks.Visible := false;
  // series.MultiBar := mbStacked;
  // series.ShowInLegend := true;
  // series.BarWidthPercent := 98;
  // series.SideMargins := false;
  //

  var
    accumTime: double := 0;

  for var i := 0 to high(self.data) do
  begin

    ch.SeriesList[0].AddXY(accumTime, self.data[i].energy);

    accumTime := accumTime + self.data[i].time;

    ch.SeriesList[0].AddXY(accumTime, self.data[i].energy);

  end;

end;

procedure TEnergyTimePainter.SetTimeEnergy(time, energy: Tarray<double>);
begin

  if Length(time) <> Length(energy) then
    raise Exception.Create('����� �������� �� �����');

  self.data := [];

  var
    bufElem: TEnergyTimePainter.TElem;

  for var i := Low(time) to High(time) do
  begin
    bufElem.time := time[i];
    bufElem.energy := energy[i];
    self.data := self.data + [bufElem];
  end;

end;

end.
