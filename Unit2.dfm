object Form2: TForm2
  Left = 0
  Top = 0
  Caption = #1054#1073#1079#1086#1088' '#1075#1088#1072#1092#1080#1082#1086#1074
  ClientHeight = 480
  ClientWidth = 727
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Chart1: TChart
    Left = 6
    Top = 8
    Width = 707
    Height = 393
    Legend.Visible = False
    Title.Font.Height = -19
    Title.Text.Strings = (
      'TChart')
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMinimum = False
    View3D = False
    Color = clWhite
    TabOrder = 0
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object Series1: TAreaSeries
      SeriesColor = clLime
      AreaChartBrush.Color = clGray
      AreaChartBrush.BackColor = clDefault
      DrawArea = True
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object Button1: TButton
    Left = 568
    Top = 414
    Width = 145
    Height = 25
    Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1080#1079' Excel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = Button1Click
  end
  object StringGrid1: TStringGrid
    Left = 784
    Top = 233
    Width = 320
    Height = 120
    TabOrder = 2
    Visible = False
  end
  object ColorBox1: TColorBox
    Left = 8
    Top = 417
    Width = 89
    Height = 22
    Selected = clLime
    Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor]
    TabOrder = 3
    OnChange = ColorBox1Change
  end
  object Button2: TButton
    Left = 112
    Top = 415
    Width = 121
    Height = 25
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1075#1088#1072#1092#1080#1082#1072
    TabOrder = 4
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 239
    Top = 415
    Width = 106
    Height = 25
    Caption = #1054#1089#1100' Y'
    TabOrder = 5
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 351
    Top = 415
    Width = 106
    Height = 25
    Caption = #1054#1089#1100' X'
    TabOrder = 6
    OnClick = Button4Click
  end
  object CheckBox1: TCheckBox
    Left = 465
    Top = 418
    Width = 56
    Height = 17
    Caption = #1051#1080#1085#1080#1080
    Checked = True
    State = cbChecked
    TabOrder = 7
    OnClick = CheckBox1Click
  end
  object Button5: TButton
    Left = 112
    Top = 446
    Width = 121
    Height = 25
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1084#1072#1082#1089'.'
    TabOrder = 8
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 568
    Top = 447
    Width = 145
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1073#1091#1092#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = Button6Click
  end
end
